<?php

use PHPUnit\Framework\TestCase;

require_once('src/BidRule.php');

class BidRuleTest extends TestCase
{

    /**
     * 報價廠商家數驗證
     * checkNumberOfCompanysIllegal()
     *1.採購方式是公開招標，則第 1 次開標至少需要 3 家廠商
     * => 傳回 false，符合規定
     */
    public function testPublicBiddingAndAtLeast3ManufacturersAreRequiredForTheFirstBid(): void
    {
        //Arrange — 初始化
        $NoBidRule = false;
        $BuyMethod = 'A';
        $TimesOfBid = 1;
        $NumberOfCompanies = 3;

        $Budget = 30000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkNumberOfCompanysIllegal($NoBidRule, $BuyMethod, $TimesOfBid, $NumberOfCompanies);

        //Assert — 驗證結果
        $this->assertEquals(false, $Check);
    }

    /**
     * 報價廠商家數驗證
     * checkNumberOfCompanysIllegal()
     *2.採購方式是公開招標，廠商家數應符合不同的預算金額範圍下的最低要求
     * => 傳回 false，符合規定
     * => 傳回 false，符合規定
     */
    public function testPublicBiddingTheNumberOfManufacturersShouldMeetTheMinimumRequirementsUnderDifferentBudgetRanges(): void
    {
        //Arrange — 初始化
        $NoBidRule = false;
        $BuyMethod = 'B';
        $TimesOfBid = 1;
        $NumberOfCompanies = 2;

        $Budget = 30000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkNumberOfCompanysIllegal($NoBidRule, $BuyMethod, $TimesOfBid, $NumberOfCompanies);

        //Assert — 驗證結果
        $this->assertEquals(false, $Check);

        //Arrange — 初始化
        $NoBidRule = false;
        $BuyMethod = 'B';
        $TimesOfBid = 1;
        $NumberOfCompanies = 3;

        $Budget = 100000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkNumberOfCompanysIllegal($NoBidRule, $BuyMethod, $TimesOfBid, $NumberOfCompanies);

        //Assert — 驗證結果
        $this->assertEquals(false, $Check);
    }

    /**
     * 報價廠商家數驗證
     * checkNumberOfCompanysIllegal()
     *3.採購方式不是公開招標也不是公開取得報價單，可跳過上述的規定
     *
     */
    public function testItNotAPublicBiddingNorAPublicQuotationAndTheAboveRegulationsCanBeSkipped()
    {
        //Arrange — 初始化
        $NoBidRule = false;
        $BuyMethod = 'C';
        $TimesOfBid = 1;
        $NumberOfCompanies = 1;

        $Budget = 30000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkNumberOfCompanysIllegal($NoBidRule, $BuyMethod, $TimesOfBid, $NumberOfCompanies);

        //Assert — 驗證結果
        $this->assertEquals(true, $Check);
    }

    /**
     * 報價廠商家數驗證
     * checkNumberOfCompanysIllegal()
     *4.例外狀況，可以透過參數跳過上述的規定
     * => 傳回 false，符合規定
     */
    public function testExceptionsCanSkipTheAboveRegulationsThroughParameters()
    {
        //Arrange — 初始化
        $NoBidRule = false;
        $BuyMethod = 'C';
        $TimesOfBid = 1;
        $NumberOfCompanies = 1;

        $Budget = 30000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkNumberOfCompanysIllegal($NoBidRule, $BuyMethod, $TimesOfBid, $NumberOfCompanies);

        //Assert — 驗證結果
        $this->assertEquals(false, $Check);
    }

    /**
     * 報價廠商家數驗證
     * checkNumberOfCompanysIllegal()
     * 在公開取得報價單的採購方式中，不同的預算金額範圍有最低的報價廠商家數限制
     * 1.總預算在 30,000 元以上，未達 100,000 元，至少需要 2 家廠商
     * => 傳回 2
     */
    public function testTheTotalBudgetIsMoreThanNT30000ButLessThanNT100000AtLeast2ManufacturersAreNeeded()
    {
        //Arrange — 初始化
        $Budget = 30000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->getMinNumberOfCompanysByBuymethod();

        //Assert — 驗證結果
        $this->assertEquals(2, $Check);
    }

    /**
     * 在公開取得報價單的採購方式中，不同的預算金額範圍有最低的報價廠商家數限制
     * getMinNumberOfCompanysByBuymethod()
     * 2.總預算在 100,000 元以上，未達 500,000 元，至少需要 3 家廠商
     * => 傳回 3
     */
    public function testTheTotalBudgetIsMoreThanNT100000ButLessThanNT500000AtLeast3ManufacturersAreNeeded()
    {
        //Arrange — 初始化
        $Budget = 100000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->getMinNumberOfCompanysByBuymethod();

        //Assert — 驗證結果
        $this->assertEquals(3, $Check);
    }

    /**
     * 在公開取得報價單的採購方式中，不同的預算金額範圍有最低的報價廠商家數限制
     * getMinNumberOfCompanysByBuymethod()
     * 3.其他狀況則不限制
     * => 傳回 99
     */
    public function testOtherConditionsAreNotRestricted()
    {
        //Arrange — 初始化
        $Budget = 0;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->getMinNumberOfCompanysByBuymethod();

        //Assert — 驗證結果
        $this->assertEquals(99, $Check);
    }

    /**
     * 決標金額驗證
     * checkBidPriceIllegal()
     * 1.有底價金額的情況下，決標金額不能超過底價金額
     * => 傳回 false，符合規定
     */
    public function testIfThereIsAReservePriceAmountTheWinningBidAmountCannotExceedTheReservePriceAmount()
    {
        //Arrange — 初始化
        $BasePrice = 80000;
        $BidPrice = 79999;

        $Budget = 100000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkBidPriceIllegal($BasePrice, $BidPrice);

        //Assert — 驗證結果
        $this->assertEquals(false, $Check);
    }

    /**
     * 決標金額驗證
     * checkBidPriceIllegal()
     * 2.無底價金額的情況下，決標金額不能超過底價金額
     * => 傳回 false，符合規定
     */
    public function testIfThereIsNoAReservePriceAmountTheWinningBidAmountCannotExceedTheReservePriceAmount()
    {
        //Arrange — 初始化
        $BasePrice = 0;
        $BidPrice = 79999;

        $Budget = 100000;
        $BidRule = new BidRule($Budget);

        //Act — 行為，測試對象的執行過程
        $Check = $BidRule->checkBidPriceIllegal($BasePrice, $BidPrice);

        //Assert — 驗證結果
        $this->assertEquals(false, $Check);
    }
}
