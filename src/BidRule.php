<?php
class BidRule
{
    /**
     * 預算金額
     * @var integer
     */
    private $nBudget = 0;

    public function __construct(int $budget)
    {
        $this->nBudget = $budget;
    }

    /**
     * 檢查參加比價的廠商家數是否不合法
     * 1.不限制比議價規則一律當作合法
     * 2.參加比價的廠商家數在3以上當作合法
     * 3.採購方式是公開招標，第1次開標至少需要3家廠商
     * 4.採購方式是公開取得，依照預算金額區間判斷
     * @param  boolean $nobidrule        是否不限制比議價規則
     * @param  string  $buymethod        採購方式
     * @param  integer $timesofbid       目前比價次數
     * @param  integer $numberofcompanys 目前有幾家廠商參加比價
     * @return boolean                   true(不合法)/false(合法)
     */
    public function checkNumberOfCompanysIllegal(bool $nobidrule, string $buymethod, int $timesofbid, int $numberofcompanys)
    {
        $boolCheck = false;

        if ($nobidrule) {
            $boolCheck = false;
        } else if ($numberofcompanys >= 3) {
            $boolCheck = false;
        } else if ($buymethod == 'A' && $timesofbid == 1) {
            $boolCheck = true;
        } else if ($buymethod == 'B' && $numberofcompanys < $this->getMinNumberOfCompanysByBuymethod()) {
            $boolCheck = true;
        }

        return $boolCheck;
    }

    /**
     * 採購方式是公開取得，
     * 1.總預算在30,000元以上，未達100,000元，至少需要2家廠商
     * 2.總預算在100,000元以上，未達500,000元，至少需要3家廠商
     * @return [type] [description]
     */
    public function getMinNumberOfCompanysByBuymethod(): int
    {
        $nMinNum = 99;

        if ($this->nBudget >= 30000 && $this->nBudget < 100000) {
            $nMinNum = 2;
        }
        if ($this->nBudget >= 100000 && $this->nBudget < 500000) {
            $nMinNum = 3;
        }

        return $nMinNum;
    }

    /**
     * 檢查決標的金額是否合法
     * 1.有底價的狀況下，決標金額不得超過底價金額
     * 2.決標金額不得超過預算金額
     * @param  [type]  $basciprice 底價金額
     * @param  integer $bidprice   決標金額
     * @return [type]              true(不合法)/false(合法)
     */
    public function checkBidPriceIllegal($basciprice, int $bidprice)
    {
        $boolCheck = false;

        if (intval($basciprice) > 0) {
            if ($bidprice > $basciprice) {
                $boolCheck = true;
            }
        } else if ($bidprice > $this->nBudget) {
            $boolCheck = true;
        }

        return $boolCheck;
    }
}
